package net.openhft.chronicle.threads;

import net.openhft.chronicle.threads.api.InvalidEventHandlerException;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Rob Austin.
 */
public class EventGroupTest {

    @Test
    public void testSimpleEventGroupTest() throws Exception {

        final AtomicInteger value = new AtomicInteger();

        try (final EventGroup eventGroup = new EventGroup(true)) {
            eventGroup.start();
            eventGroup.addHandler(() -> {
                if (value.get() == 10)
                    // throw this if you don't wish to be called back
                    throw new InvalidEventHandlerException();
                value.incrementAndGet();
                return true;
            });

            final long start = System.currentTimeMillis();
            while (value.get() != 10) {
                Thread.sleep(1);
            }

            Assert.assertTrue(System.currentTimeMillis() < start + TimeUnit.SECONDS.toMillis(5));

            for (int i = 0; i < 10; i++) {
                Assert.assertEquals(10, value.get());
                Thread.sleep(1);
            }
        }
    }

}
